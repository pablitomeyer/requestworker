if [ $# -ne 2 ]; then
    echo "Usage: create_files.sh <number of files> <output_path>"
	exit
fi
for value in $(seq 1 $1);
do
    echo "<html><body><h1>Test file number $value</h1></body></html>" > $2/file_$value
done
