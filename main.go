package main

import (
	"github.com/gorilla/http"
	"fmt"
	"io/ioutil"
	"time"
	"flag"
	"strings"
)

type RequestInfo struct {
	status 	int
	elapsed time.Duration
	err 	error
	body 	string
	url		string
}

func (o *RequestInfo) isSuccess() bool {
	if o.err != nil {
		return false
	}
	return o.status == 200
}

func makeRequest(url string) RequestInfo{
	result := RequestInfo{}
	result.url = url
	start := time.Now()
	
	status, _, r, err := http.DefaultClient.Get(url, nil)

	if err != nil {
		result.err = err
		return result
	}

	result.elapsed = time.Since(start)
	result.status = status.Code
	if !status.IsSuccess() {
		return result
	}

	defer r.Close()
	respBody, err := ioutil.ReadAll(r)
	if err != nil {
		result.err = err
		return result
	}
	result.body = string(respBody)
	return result
}

func worker(id int, jobs <-chan string, results chan<- RequestInfo, debug bool) {
    for j := range jobs {
		if debug {
			fmt.Println("worker", id, "started  job", j)
		}
		r := makeRequest(j)
		
		if debug {
			fmt.Println("worker", id, "finished job", j)
		}
        results <- r
    }
}

func start(jobsCount int, workersCount int, baseUrl string, filePattern string, debug bool) {
	jobs := make(chan string, jobsCount)
	results := make(chan RequestInfo, jobsCount)
	
    for w := 1; w <= workersCount; w++ {
        go worker(w, jobs, results, debug)
    }

	if !strings.HasSuffix(baseUrl, "/") {
		baseUrl += "/"
	}

	fullUrlPattern := "%s" + filePattern

    for j := 1; j <= jobsCount; j++ {
        jobs <- fmt.Sprintf(fullUrlPattern, baseUrl, j)
    }
    close(jobs)

	success := 0
	errorCount := 0
	var meanElapsedTime time.Duration

    for a := 1; a <= jobsCount; a++ {
		r :=  <-results

		if r.isSuccess() {
			success++
			meanElapsedTime += r.elapsed
		} else {
			errorCount++
		}

		if debug {
			fmt.Printf("Result url: %s\n", r.url)
			if r.err != nil {
				fmt.Println("Error ", r.err.Error() )
			} else {
				fmt.Printf("Response %d in %s\n", r.status, r.elapsed)
				fmt.Printf("Body: %s\n", r.body)
			}
		}
	}
	
	fmt.Printf("Successful requests: %d\n", success)
	if (success > 0) {
		ms := int64(meanElapsedTime / time.Millisecond)
		mean := time.Duration(ms / int64(success))
		fmt.Printf("Mean Sucessful request time: %s\n", mean)
	}
	fmt.Printf("Invalid requests: %d\n", errorCount)
	
}

func main() {
	jobs := flag.Int("req", 5, "the amount of request to make")
	workers := flag.Int("worker", 5, "the amount of workers to create")
	url := flag.String("baseurl", "http://localhost:8080/", "base URL for the requests")
	filePattern := flag.String("pattern", "file_%d", "file name pattern. Must include a %d argument")
	debug := flag.Bool("debug", false, "print debug info")

	flag.Parse()

	s := time.Now()
	start(*jobs, *workers, *url, *filePattern, *debug)
	fmt.Printf("Finished in %s\n", time.Since(s))
}
